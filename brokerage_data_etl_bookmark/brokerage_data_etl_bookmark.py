import sys
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job
from awsglue.dynamicframe import DynamicFrameCollection
from awsglue.dynamicframe import DynamicFrame
import re

# Script generated for node Custom Transform
def write_responses_responsevalue__audit(glueContext, dfc) -> DynamicFrameCollection:
    glueContext.write_dynamic_frame.from_jdbc_conf(
        frame=dfc.select(list(dfc.keys())[0]),
        catalog_connection="redshift-ajg",
        connection_options={
            "dbtable": "responses_responsevalue__audit",
            "database": "indio",
            "bulkSize": "2"
        },
        redshift_tmp_dir=args["TempDir"],
    )


args = getResolvedOptions(sys.argv, ["JOB_NAME",  "brokerage_id"])
sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args["JOB_NAME"], args)

# Script generated for node AWS Glue Data Catalog
AWSGlueDataCatalog_node1667407086584 = glueContext.create_dynamic_frame.from_catalog(
    database="production_414",
    table_name="brokerage_gallagher_public_responses_responsevalue__audit",
    transformation_ctx="AWSGlueDataCatalog_node1667407086584",
    additional_options = {"jobBookmarkKeys": ["tx_issued_at", "event_id"], "jobBookmarkKeysSortOrder":"asc", 'hashexpression': 'id', 'hashpartitions': "4"},
)

# Script generated for node Filter
Filter_node1667407096576 = Filter.apply(
    frame=AWSGlueDataCatalog_node1667407086584,
    f=lambda row: (row["brokerage_id"] == int(args["brokerage_id"])),
    transformation_ctx="Filter_node1667407096576",
)

# Script generated for node Drop Fields
DropFields_node1667407107050 = DropFields.apply(
    frame=Filter_node1667407096576,
    paths=["brokerage_id"],
    transformation_ctx="DropFields_node1667407107050",
)

# Script generated for node Custom Transform
CustomTransform_node1667407118134 = write_responses_responsevalue__audit(
    glueContext,
    DynamicFrameCollection(
        {"DropFields_node1667407107050": DropFields_node1667407107050}, glueContext
    ),
)

job.commit()
