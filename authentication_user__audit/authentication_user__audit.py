import sys
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job

## @params: [TempDir, JOB_NAME]
args = getResolvedOptions(sys.argv, ['TempDir','JOB_NAME'])

sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args['JOB_NAME'], args)

datasource0 = glueContext.create_dynamic_frame.from_catalog(
    database = "indio-production-backup",
    table_name = "authentication_user__audit",
    transformation_ctx = "datasource0",
    additional_options = {"jobBookmarkKeys":["tx_issued_at", "event_id"],"jobBookmarkKeysSortOrder":"asc", 'hashexpression': 'id','hashpartitions': '9'}
)

applymapping1 = ApplyMapping.apply(
    frame = datasource0, mappings = [
        ("event_id", "int", "event_id", "int"),
        ("event_kind", "string", "event_kind", "string"),
        ("tx_issued_at", "timestamp", "tx_issued_at", "timestamp"),
        ("id", "int", "id", "int"),
        ("account_type", "string", "account_type", "string"),
        ("email", "string", "email", "string"),
        ("password", "string", "password", "string"),
        ("first_name", "string", "first_name", "string"),
        ("last_name", "string", "last_name", "string"),
        ("phone", "string", "phone", "string"),
        ("is_superuser", "boolean", "is_superuser", "boolean"),
        ("confirmed", "boolean", "confirmed", "boolean"),
        (
            "accepted_privacy_policy",
            "timestamp",
            "accepted_privacy_policy",
            "timestamp",
        ),
        ("date_joined", "timestamp", "date_joined", "timestamp"),
        ("last_login", "timestamp", "last_login", "timestamp"),
        ("last_request", "timestamp", "last_request", "timestamp"),
        ("timezone", "string", "timezone", "string"),
        ("brokerage_id", "int", "brokerage_id", "int"),
        ("tx_id", "int", "tx_id", "int"),
    ],
    transformation_ctx = "applymapping1",)

datasink2 = glueContext.write_dynamic_frame.from_jdbc_conf(
    frame = applymapping1, catalog_connection = "analytics",
    connection_options = {"dbtable": "authentication_user__audit", "database": "indio", "bulkSize": "9"},
    redshift_tmp_dir = args["TempDir"],
    transformation_ctx = "datasink2")

job.commit()
