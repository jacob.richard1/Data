import sys
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job

## @params: [TempDir, JOB_NAME]
args = getResolvedOptions(sys.argv, ['TempDir','JOB_NAME'])

sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args['JOB_NAME'], args)

datasource0 = glueContext.create_dynamic_frame.from_catalog(
    database = "indio-production-backup",
    table_name = "clients_client__audit",
    transformation_ctx = "datasource0",
    additional_options = {"jobBookmarkKeys":["tx_issued_at", "event_id"],"jobBookmarkKeysSortOrder":"asc", 'hashexpression': 'id','hashpartitions': '9'}
)

applymapping1 = ApplyMapping.apply(
    frame = datasource0, mappings = [
     ("event_id", "int", "event_id", "int"),
     ("event_kind", "string", "event_kind", "string"),
     ("tx_issued_at", "timestamp", "tx_issued_at", "timestamp"),
     ("id", "int", "id", "int"),
     ("created", "timestamp", "created", "timestamp"),
     ("updated", "timestamp", "updated", "timestamp"),
     ("type", "string", "type", "string"),
     ("opportunity_type", "string", "opportunity_type", "string"),
     ("name", "string", "name", "string"),
     ("is_active", "boolean", "is_active", "boolean"),
     ("ssn", "string", "ssn", "string"),
     ("fein", "string", "fein", "string"),
     ("ams_id", "string", "ams_id", "string"),
     ("ams_lookup_code", "string", "ams_lookup_code", "string"),
     ("salesforce_account_id", "string", "salesforce_account_id", "string"),
     ("last_activity", "timestamp", "last_activity", "timestamp"),
     ("can_view_policies", "boolean", "can_view_policies", "boolean"),
     ("is_pde", "boolean", "is_pde", "boolean"),
     (
         "client_contact_default_language",
         "string",
         "client_contact_default_language",
         "string",
     ),
     ("brokerage_id", "int", "brokerage_id", "int"),
     ("created_by_id", "int", "created_by_id", "int"),
     ("primary_user_id", "int", "primary_user_id", "int"),
     ("sic_code_id", "string", "sic_code_id", "string"),
     ("tx_id", "int", "tx_id", "int"),
    ],
    transformation_ctx = "applymapping1")

datasink2 = glueContext.write_dynamic_frame.from_jdbc_conf(
    frame = applymapping1, catalog_connection = "analytics", 
    connection_options = {"dbtable": "clients_client__audit", "database": "indio", "bulkSize": "9"},
    redshift_tmp_dir = args["TempDir"], 
    transformation_ctx = "datasink2")

job.commit()