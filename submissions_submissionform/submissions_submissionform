import sys
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job
from awsglue.dynamicframe import DynamicFrame


args = getResolvedOptions(sys.argv, ['TempDir','JOB_NAME'])

sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args['JOB_NAME'], args)


# signature requests
signature_requests = glueContext.create_dynamic_frame.from_catalog(
	database = "production",
	table_name = "indio_public_signatures_signaturerequest")

signature_requests = ApplyMapping.apply(
	frame = signature_requests,
	mappings = [
		("id", "int", "esignature_request_id", "int"),
		("status", "string", "signature_status", "string"),
        ("created", "timestamp", "signature_request_created", "timestamp"),
        ("updated", "timestamp", "signature_request_updated", "timestamp")])

# submission forms
submission_forms = glueContext.create_dynamic_frame.from_catalog(
	database = "production",
	table_name = "indio_public_submissions_submissionform")

submission_forms = ApplyMapping.apply(
	frame = submission_forms,
	mappings = [("submitted_date", "timestamp", "submitted_date", "timestamp"),
		("answered", "int", "answered", "int"),
		("name_alias", "string", "name_alias", "string"),
		("broker_only", "boolean", "broker_only", "boolean"),
		("form_bundle_id", "string", "form_bundle_id", "string"),
		("form_id", "int", "form_id", "int"),
		("total", "int", "total", "int"),
		("category_alias", "string", "category_alias", "string"),
		("id", "int", "id", "int"),
		("signature_request_id", "int", "signature_request_id", "int"),
		("verification_enabled", "boolean", "verification_enabled", "boolean"),
		("comparison_pdf", "string", "comparison_pdf", "string"),
		("attachment_count", "int", "attachment_count", "int")])


submission_forms_df = submission_forms.toDF()
signature_requests_df = signature_requests.toDF()
	
submission_forms_df = submission_forms_df.join(signature_requests_df, submission_forms_df.signature_request_id == signature_requests_df.esignature_request_id, how="left")

glueContext.write_dynamic_frame.from_jdbc_conf(
	frame = DynamicFrame.fromDF(submission_forms_df, glueContext, "submission_forms_dynamic_frame"),
	catalog_connection = "analytics",
	connection_options = {"dbtable": "submissions_submissionform", "database": "indio", "preactions":"TRUNCATE TABLE submissions_submissionform", "bulkSize": "2"},
	redshift_tmp_dir = args["TempDir"])


job.commit()
