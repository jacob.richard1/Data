import sys
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job
from awsglue.dynamicframe import DynamicFrameCollection
from awsglue.dynamicframe import DynamicFrame
import re

# Script generated for node write_submissions_submission
def write_submissions_submission(glueContext, dfc) -> DynamicFrameCollection:
    glueContext.write_dynamic_frame.from_jdbc_conf(
        frame=dfc.select(list(dfc.keys())[0]),
        catalog_connection="redshift-ajg",
        connection_options={
            "dbtable": "submissions_submission",
            "database": "indio",
            "preactions": "TRUNCATE TABLE submissions_submission",
        },
        redshift_tmp_dir=args["TempDir"],
    )

# Script generated for node Custom Transform
def write_submissions_formcomment(glueContext, dfc) -> DynamicFrameCollection:
    glueContext.write_dynamic_frame.from_jdbc_conf(
        frame=dfc.select(list(dfc.keys())[0]),
        catalog_connection="redshift-ajg",
        connection_options={
            "dbtable": "submissions_formcomment",
            "database": "indio",
            "preactions": "TRUNCATE TABLE submissions_formcomment",
        },
        redshift_tmp_dir=args["TempDir"],
    )

# Script generated for node write_submissions_formbundle
def write_submissions_formbundle(glueContext, dfc) -> DynamicFrameCollection:
    glueContext.write_dynamic_frame.from_jdbc_conf(
        frame=dfc.select(list(dfc.keys())[0]),
        catalog_connection="redshift-ajg",
        connection_options={
            "dbtable": "submissions_formbundle",
            "database": "indio",
            "preactions": "TRUNCATE TABLE submissions_formbundle",
        },
        redshift_tmp_dir=args["TempDir"],
    )

# Script generated for node Custom Transform
def write_clients_client_assignees(glueContext, dfc) -> DynamicFrameCollection:
    glueContext.write_dynamic_frame.from_jdbc_conf(
        frame=dfc.select(list(dfc.keys())[0]),
        catalog_connection="redshift-ajg",
        connection_options={
            "dbtable": "clients_client_assignees",
            "database": "indio",
            "preactions": "TRUNCATE TABLE clients_client_assignees",
        },
        redshift_tmp_dir=args["TempDir"],
    )

# Script generated for node write_responses_responserow
def write_responses_responserow(glueContext, dfc) -> DynamicFrameCollection:
    glueContext.write_dynamic_frame.from_jdbc_conf(
        frame=dfc.select(list(dfc.keys())[0]),
        catalog_connection="redshift-ajg",
        connection_options={
            "dbtable": "responses_responserow",
            "database": "indio",
            "preactions": "TRUNCATE TABLE responses_responserow",
        },
        redshift_tmp_dir=args["TempDir"],
    )


# Script generated for node write_regions_region
def write_regions_region(glueContext, dfc) -> DynamicFrameCollection:
    glueContext.write_dynamic_frame.from_jdbc_conf(
        frame=dfc.select(list(dfc.keys())[0]),
        catalog_connection="redshift-ajg",
        connection_options={
            "dbtable": "regions_region",
            "database": "indio",
            "preactions": "TRUNCATE TABLE regions_region",
        },
        redshift_tmp_dir=args["TempDir"],
    )


# Script generated for node write_client_entities_cliententity
def write_client_entities_cliententity(glueContext, dfc) -> DynamicFrameCollection:
    glueContext.write_dynamic_frame.from_jdbc_conf(
        frame=dfc.select(list(dfc.keys())[0]),
        catalog_connection="redshift-ajg",
        connection_options={
            "dbtable": "client_entities_cliententity",
            "database": "indio",
            "preactions": "TRUNCATE TABLE client_entities_cliententity",
        },
        redshift_tmp_dir=args["TempDir"],
    )


# Script generated for node write_submissions_submissionform
def write_submissions_submissionform(glueContext, dfc) -> DynamicFrameCollection:
    glueContext.write_dynamic_frame.from_jdbc_conf(
        frame=dfc.select(list(dfc.keys())[0]),
        catalog_connection="redshift-ajg",
        connection_options={
            "dbtable": "submissions_submissionform",
            "database": "indio",
            "preactions": "TRUNCATE TABLE submissions_submissionform",
        },
        redshift_tmp_dir=args["TempDir"],
    )


# Script generated for node write_responses_responsevalue
def write_responses_responsevalue(glueContext, dfc) -> DynamicFrameCollection:
    glueContext.write_dynamic_frame.from_jdbc_conf(
        frame=dfc.select(list(dfc.keys())[0]),
        catalog_connection="redshift-ajg",
        connection_options={
            "dbtable": "responses_responsevalue",
            "database": "indio",
            "preactions": "TRUNCATE TABLE responses_responsevalue",
        },
        redshift_tmp_dir=args["TempDir"],
    )


# Script generated for node write_submissions_submission_response_rows
def write_submissions_submission_response_rows(
    glueContext, dfc
) -> DynamicFrameCollection:
    glueContext.write_dynamic_frame.from_jdbc_conf(
        frame=dfc.select(list(dfc.keys())[0]),
        catalog_connection="redshift-ajg",
        connection_options={
            "dbtable": "submissions_submission_response_rows",
            "database": "indio",
            "preactions": "TRUNCATE TABLE submissions_submission_response_rows",
        },
        redshift_tmp_dir=args["TempDir"],
    )

# Script generated for node Custom Transform clients_clientcontact
def write_clients_clientcontact(glueContext, dfc) -> DynamicFrameCollection:
    glueContext.write_dynamic_frame.from_jdbc_conf(
        frame=dfc.select(list(dfc.keys())[0]),
        catalog_connection="redshift-ajg",
        connection_options={
            "dbtable": "clients_clientcontact",
            "database": "indio",
            "preactions": "TRUNCATE TABLE clients_clientcontact",
        },
        redshift_tmp_dir=args["TempDir"],
    )


# Script generated for node write_clients_client_regions
def write_clients_client_regions(glueContext, dfc) -> DynamicFrameCollection:
    glueContext.write_dynamic_frame.from_jdbc_conf(
        frame=dfc.select(list(dfc.keys())[0]),
        catalog_connection="redshift-ajg",
        connection_options={
            "dbtable": "clients_client_regions",
            "database": "indio",
            "preactions": "TRUNCATE TABLE clients_client_regions",
        },
        redshift_tmp_dir=args["TempDir"],
    )


# Script generated for node write_brokers_broker
def write_brokers_broker(glueContext, dfc) -> DynamicFrameCollection:
    glueContext.write_dynamic_frame.from_jdbc_conf(
        frame=dfc.select(list(dfc.keys())[0]),
        catalog_connection="redshift-ajg",
        connection_options={
            "dbtable": "brokers_broker",
            "database": "indio",
            "preactions": "TRUNCATE TABLE brokers_broker",
        },
        redshift_tmp_dir=args["TempDir"],
    )


# Script generated for node write_brokers_broker_regions
def write_brokers_broker_regions(glueContext, dfc) -> DynamicFrameCollection:
    glueContext.write_dynamic_frame.from_jdbc_conf(
        frame=dfc.select(list(dfc.keys())[0]),
        catalog_connection="redshift-ajg",
        connection_options={
            "dbtable": "brokers_broker_regions",
            "database": "indio",
            "preactions": "TRUNCATE TABLE brokers_broker_regions",
        },
        redshift_tmp_dir=args["TempDir"],
    )


# Script generated for node write_form_builder_formfield
def write_form_builder_formfield(glueContext, dfc) -> DynamicFrameCollection:
    glueContext.write_dynamic_frame.from_jdbc_conf(
        frame=dfc.select(list(dfc.keys())[0]),
        catalog_connection="redshift-ajg",
        connection_options={
            "dbtable": "form_builder_formfield",
            "database": "indio",
            "preactions": "TRUNCATE TABLE form_builder_formfield",
        },
        redshift_tmp_dir=args["TempDir"],
    )


# Script generated for node write_responses_response
def write_responses_response(glueContext, dfc) -> DynamicFrameCollection:
    glueContext.write_dynamic_frame.from_jdbc_conf(
        frame=dfc.select(list(dfc.keys())[0]),
        catalog_connection="redshift-ajg",
        connection_options={
            "dbtable": "responses_response",
            "database": "indio",
            "preactions": "TRUNCATE TABLE responses_response",
        },
        redshift_tmp_dir=args["TempDir"],
    )


# Script generated for node write_clients_client
def write_clients_client(glueContext, dfc) -> DynamicFrameCollection:
    glueContext.write_dynamic_frame.from_jdbc_conf(
        frame=dfc.select(list(dfc.keys())[0]),
        catalog_connection="redshift-ajg",
        connection_options={
            "dbtable": "clients_client",
            "database": "indio",
            "preactions": "TRUNCATE TABLE clients_client",
        },
        redshift_tmp_dir=args["TempDir"],
    )


# Script generated for node write_authentication_user
def write_authentication_user(glueContext, dfc) -> DynamicFrameCollection:
    glueContext.write_dynamic_frame.from_jdbc_conf(
        frame=dfc.select(list(dfc.keys())[0]),
        catalog_connection="redshift-ajg",
        connection_options={
            "dbtable": "authentication_user",
            "database": "indio",
            "preactions": "TRUNCATE TABLE authentication_user",
        },
        redshift_tmp_dir=args["TempDir"],
    )


# Script generated for node write_form_builder_form
def write_form_builder_form(glueContext, dfc) -> DynamicFrameCollection:
    glueContext.write_dynamic_frame.from_jdbc_conf(
        frame=dfc.select(list(dfc.keys())[0]),
        catalog_connection="redshift-ajg",
        connection_options={
            "dbtable": "form_builder_form",
            "database": "indio",
            "preactions": "TRUNCATE TABLE form_builder_form",
        },
        redshift_tmp_dir=args["TempDir"],
    )


# Script generated for node write_submissions_submissionformresponserow
def write_submissions_submissionformresponserow(
    glueContext, dfc
) -> DynamicFrameCollection:
    glueContext.write_dynamic_frame.from_jdbc_conf(
        frame=dfc.select(list(dfc.keys())[0]),
        catalog_connection="redshift-ajg",
        connection_options={
            "dbtable": "submissions_submissionformresponserow",
            "database": "indio",
            "preactions": "TRUNCATE TABLE submissions_submissionformresponserow",
        },
        redshift_tmp_dir=args["TempDir"],
    )


# Script generated for node write_schedules_schedule
def write_schedules_schedule(glueContext, dfc) -> DynamicFrameCollection:
    glueContext.write_dynamic_frame.from_jdbc_conf(
        frame=dfc.select(list(dfc.keys())[0]),
        catalog_connection="redshift-ajg",
        connection_options={
            "dbtable": "schedules_schedule",
            "database": "indio",
            "preactions": "TRUNCATE TABLE schedules_schedule",
        },
        redshift_tmp_dir=args["TempDir"],
    )


# Script generated for node write_schedules_scheduleentity
def write_schedules_scheduleentity(glueContext, dfc) -> DynamicFrameCollection:
    glueContext.write_dynamic_frame.from_jdbc_conf(
        frame=dfc.select(list(dfc.keys())[0]),
        catalog_connection="redshift-ajg",
        connection_options={
            "dbtable": "schedules_scheduleentity",
            "database": "indio",
            "preactions": "TRUNCATE TABLE schedules_scheduleentity",
        },
        redshift_tmp_dir=args["TempDir"],
    )


args = getResolvedOptions(sys.argv, ["JOB_NAME", "brokerage_id"])
sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args["JOB_NAME"], args)

# Script generated for node authentication_user
authentication_user_node1649876352521 = glueContext.create_dynamic_frame.from_catalog(
    database="production",
    table_name="indio_public_authentication_user",
    transformation_ctx="authentication_user_node1649876352521",
)

# Script generated for node brokers_broker
brokers_broker_node1649876578960 = glueContext.create_dynamic_frame.from_catalog(
    database="production",
    table_name="indio_public_brokers_broker",
    transformation_ctx="brokers_broker_node1649876578960",
)

# Script generated for node clients_client
clients_client_node1649876838227 = glueContext.create_dynamic_frame.from_catalog(
    database="production",
    table_name="indio_public_clients_client",
    transformation_ctx="clients_client_node1649876838227",
)

# Script generated for node client_entities_cliententity
client_entities_cliententity_node1649877100779 = (
    glueContext.create_dynamic_frame.from_catalog(
        database="production",
        table_name="indio_public_client_entities_cliententity",
        transformation_ctx="client_entities_cliententity_node1649877100779",
    )
)

# Script generated for node brokerages_brokerage_form
brokerages_brokerage_form_node1649877488549 = (
    glueContext.create_dynamic_frame.from_catalog(
        database="production",
        table_name="indio_public_brokerages_brokerage_forms",
        transformation_ctx="brokerages_brokerage_form_node1649877488549",
    )
)

# Script generated for node form_builder_form
form_builder_form_node1649878282686 = glueContext.create_dynamic_frame.from_catalog(
    database="production",
    table_name="indio_public_form_builder_form",
    transformation_ctx="form_builder_form_node1649878282686",
)

# Script generated for node form_builder_formfield
form_builder_formfield_node1649878501033 = (
    glueContext.create_dynamic_frame.from_catalog(
        database="production",
        table_name="indio_public_form_builder_formfield",
        transformation_ctx="form_builder_formfield_node1649878501033",
    )
)

# Script generated for node regions_region
regions_region_node1649878647063 = glueContext.create_dynamic_frame.from_catalog(
    database="production",
    table_name="indio_public_regions_region",
    transformation_ctx="regions_region_node1649878647063",
)

# Script generated for node clients_client_regions
clients_client_regions_node1649878772765 = (
    glueContext.create_dynamic_frame.from_catalog(
        database="production",
        table_name="indio_public_clients_client_regions",
        transformation_ctx="clients_client_regions_node1649878772765",
    )
)

# Script generated for node brokers_broker_regions
brokers_broker_regions_node1649878913932 = (
    glueContext.create_dynamic_frame.from_catalog(
        database="production",
        table_name="indio_public_brokers_broker_regions",
        transformation_ctx="brokers_broker_regions_node1649878913932",
    )
)

# Script generated for node responses_responserow
responses_responserow_node1649879157296 = glueContext.create_dynamic_frame.from_catalog(
    database="production",
    table_name="indio_public_responses_responserow",
    transformation_ctx="responses_responserow_node1649879157296",
)

# Script generated for node responses_response
responses_response_node1649879323666 = glueContext.create_dynamic_frame.from_catalog(
    database="production",
    table_name="indio_public_responses_response",
    transformation_ctx="responses_response_node1649879323666",
)

# Script generated for node responses_responsevalue
responses_responsevalue_node1649879472621 = (
    glueContext.create_dynamic_frame.from_catalog(
        database="production_414",
        table_name="brokerage_gallagher_public_responses_responsevalue",
        transformation_ctx="responses_responsevalue_node1649879472621",
    )
)

# Script generated for node submissions_submission
submissions_submission_node1649879933147 = (
    glueContext.create_dynamic_frame.from_catalog(
        database="production",
        table_name="indio_public_submissions_submission",
        transformation_ctx="submissions_submission_node1649879933147",
    )
)

# Script generated for node submission_formbundle
submission_formbundle_node1649880202064 = glueContext.create_dynamic_frame.from_catalog(
    database="production",
    table_name="indio_public_submissions_formbundle",
    transformation_ctx="submission_formbundle_node1649880202064",
)

# Script generated for node submissions_submission_response_rows
submissions_submission_response_rows_node1649880414463 = (
    glueContext.create_dynamic_frame.from_catalog(
        database="production",
        table_name="indio_public_submissions_submission_response_rows",
        transformation_ctx="submissions_submission_response_rows_node1649880414463",
    )
)

# Script generated for node schedules_schedule
schedules_schedule_node1649880601712 = glueContext.create_dynamic_frame.from_catalog(
    database="production",
    table_name="indio_public_schedules_schedule",
    transformation_ctx="schedules_schedule_node1649880601712",
)

# Script generated for node submissions_submissionform
submissions_submissionform_node1649881508894 = (
    glueContext.create_dynamic_frame.from_catalog(
        database="production",
        table_name="indio_public_submissions_submissionform",
        transformation_ctx="submissions_submissionform_node1649881508894",
    )
)

# Script generated for node submissions_submissionformresponserow
submissions_submissionformresponserow_node1649885761419 = (
    glueContext.create_dynamic_frame.from_catalog(
        database="production",
        table_name="indio_public_submissions_submissionformresponserow",
        transformation_ctx="submissions_submissionformresponserow_node1649885761419",
    )
)

# Script generated for node schedules_scheduleentity
schedules_scheduleentity_node1649886037198 = (
    glueContext.create_dynamic_frame.from_catalog(
        database="production",
        table_name="indio_public_schedules_scheduleentity",
        transformation_ctx="schedules_scheduleentity_node1649886037198",
    )
)

# Script generated for node clients_client_assignees
clients_client_assignees_node1666988548566 = (
    glueContext.create_dynamic_frame.from_catalog(
        database="production",
        table_name="indio_public_clients_client_assignees",
        transformation_ctx="clients_client_assignees_node1666988548566",
    )
)
# Script generated for node clients_clientcontact
clients_clientcontact_node1666990877010 = glueContext.create_dynamic_frame.from_catalog(
    database="production",
    table_name="indio_public_clients_clientcontact",
    transformation_ctx="clients_clientcontact_node1666990877010",
)
# Script generated for node submissions_formcomment
submissions_formcomment_node1666992103407 = (
    glueContext.create_dynamic_frame.from_catalog(
        database="production",
        table_name="indio_public_submissions_formcomment",
        transformation_ctx="submissions_formcomment_node1666992103407",
    )
)

# Script generated for node filter_users
filter_users_node1649876406262 = Filter.apply(
    frame=authentication_user_node1649876352521,
    f=lambda row: (
        row["brokerage_id"] == int(args["brokerage_id"]) or bool(re.match("bot", row["account_type"]))
    ),
    transformation_ctx="filter_users_node1649876406262",
)

# Script generated for node filter_brokers
filter_brokers_node1649876617193 = Filter.apply(
    frame=brokers_broker_node1649876578960,
    f=lambda row: (row["brokerage_id"] == int(args["brokerage_id"])),
    transformation_ctx="filter_brokers_node1649876617193",
)

# Script generated for node filter_on_brokerage
filter_on_brokerage_node1649876860713 = Filter.apply(
    frame=clients_client_node1649876838227,
    f=lambda row: (row["brokerage_id"] == int(args["brokerage_id"])),
    transformation_ctx="filter_on_brokerage_node1649876860713",
)

# Script generated for node filter_forms
filter_forms_node1649877526605 = Filter.apply(
    frame=brokerages_brokerage_form_node1649877488549,
    f=lambda row: (row["brokerage_id"] == int(args["brokerage_id"])),
    transformation_ctx="filter_forms_node1649877526605",
)

# Script generated for node select_fields_fields
select_fields_fields_node1649878520347 = SelectFields.apply(
    frame=form_builder_formfield_node1649878501033,
    paths=[
        "is_locked",
        "field_caption",
        "created",
        "is_repeatable",
        "tooltip",
        "label",
        "type",
        "alias",
        "guid",
        "id",
        "tag",
        "updated",
    ],
    transformation_ctx="select_fields_fields_node1649878520347",
)

# Script generated for node filter_regions
filter_regions_node1649878668649 = Filter.apply(
    frame=regions_region_node1649878647063,
    f=lambda row: (row["brokerage_id"] == int(args["brokerage_id"])),
    transformation_ctx="filter_regions_node1649878668649",
)

# Script generated for node filter_response_values
filter_response_values_node1649879810196 = Filter.apply(
    frame=responses_responsevalue_node1649879472621,
    f=lambda row: (row["brokerage_id"] == int(args["brokerage_id"])),
    transformation_ctx="filter_response_values_node1649879810196",
)

# Script generated for node select_schedules_schedule_fields
select_schedules_schedule_fields_node1649880622626 = SelectFields.apply(
    frame=schedules_schedule_node1649880601712,
    paths=[
        "submitted_date",
        "submission_id",
        "date_imported",
        "broker_only",
        "created",
        "id",
        "updated",
    ],
    transformation_ctx="select_schedules_schedule_fields_node1649880622626",
)

# Script generated for node select_submission_form_fields
select_submission_form_fields_node1649881533507 = SelectFields.apply(
    frame=submissions_submissionform_node1649881508894,
    paths=[
        "submitted_date",
        "name_alias",
        "broker_only",
        "created",
        "form_bundle_id",
        "form_id",
        "category_alias",
        "id",
        "verification_enabled",
        "created_by_id",
        "updated",
        "attachment_count",
    ],
    transformation_ctx="select_submission_form_fields_node1649881533507",
)


# Script generated for node select_user_fields
select_user_fields_node1649876461043 = SelectFields.apply(
    frame=filter_users_node1649876406262,
    paths=[
        "account_type",
        "last_login",
        "timezone",
        "id",
        "date_joined",
        "brokerage_id",
        "email",
        "last_request",
    ],
    transformation_ctx="select_user_fields_node1649876461043",
)

# Script generated for node map_auth_fields
map_auth_fields_node1667513245257 = ApplyMapping.apply(
    frame=filter_users_node1649876406262,
    mappings=[("id", "int", "id_of_user", "int")],
    transformation_ctx="map_auth_fields_node1667513245257",
)


# Script generated for node select_brokers_fields
select_brokers_fields_node1649876646316 = SelectFields.apply(
    frame=filter_brokers_node1649876617193,
    paths=[
        "id",
        "date_invited",
        "daily_email_enabled",
        "is_active",
        "date_deactivated",
        "client_activity_emails_enabled",
        "created",
        "ams_id",
        "created_by",
        "created_by_user_id",
        "user_id",
        "broker_type",
        "brokerage_id",
        "updated",
    ],
    transformation_ctx="select_brokers_fields_node1649876646316",
)
# Script generated for node Renamed keys for Join
RenamedkeysforJoin_node1666989032763 = ApplyMapping.apply(
    frame=filter_brokers_node1649876617193,
    mappings=[
        ("brokerage_address", "string", "`(right) brokerage_address`", "string"),
        ("photo_metadata", "string", "`(right) photo_metadata`", "string"),
        ("id", "int", "`(right) id`", "int"),
    ],
    transformation_ctx="RenamedkeysforJoin_node1666989032763",
)

# Script generated for node select_clients_client_fields
select_clients_client_fields_node1649876905884 = SelectFields.apply(
    frame=filter_on_brokerage_node1649876860713,
    paths=[
        "opportunity_type",
        "ams_lookup_code",
        "is_active",
        "created",
        "ams_id",
        "type",
        "primary_user_id",
        "can_view_policies",
        "last_activity",
        "name",
        "id",
        "created_by_id",
        "updated",
        "brokerage_id",
        "sic_code_id",
    ],
    transformation_ctx="select_clients_client_fields_node1649876905884",
)

# Script generated for node select_form_fields
select_form_fields_node1649878250458 = SelectFields.apply(
    frame=filter_forms_node1649877526605,
    paths=["form_id"],
    transformation_ctx="select_form_fields_node1649878250458",
)

# Script generated for node write_form_builder_formfield
write_form_builder_formfield_node1649878559214 = write_form_builder_formfield(
    glueContext,
    DynamicFrameCollection(
        {
            "select_fields_fields_node1649878520347": select_fields_fields_node1649878520347
        },
        glueContext,
    ),
)

# Script generated for node select_region_fields
select_region_fields_node1649878704798 = SelectFields.apply(
    frame=filter_regions_node1649878668649,
    paths=[
        "imageright_code",
        "ams_lookup_code",
        "is_active",
        "created",
        "name",
        "ams_id",
        "guid",
        "is_default",
        "updated",
    ],
    transformation_ctx="select_region_fields_node1649878704798",
)

# Script generated for node select_response_values_fields
select_response_values_fields_node1649879839634 = SelectFields.apply(
    frame=filter_response_values_node1649879810196,
    paths=["last_modified_by_id", "json", "id", "type"],
    transformation_ctx="select_response_values_fields_node1649879839634",
)



# Script generated for node write_authentication_user
write_authentication_user_node1649876514733 = write_authentication_user(
    glueContext,
    DynamicFrameCollection(
        {"select_user_fields_node1649876461043": select_user_fields_node1649876461043},
        glueContext,
    ),
)



# Script generated for node map_brokers_fields
map_brokers_fields_node1649876718627 = ApplyMapping.apply(
    frame=select_brokers_fields_node1649876646316,
    mappings=[
        ("id", "int", "id", "int"),
        ("date_invited", "timestamp", "date_invited", "timestamp"),
        ("daily_email_enabled", "boolean", "daily_email_enabled", "boolean"),
        ("is_active", "boolean", "is_active", "boolean"),
        ("date_deactivated", "timestamp", "date_deactivated", "timestamp"),
        (
            "client_activity_emails_enabled",
            "boolean",
            "client_activity_emails_enabled",
            "boolean",
        ),
        ("created", "timestamp", "created", "timestamp"),
        ("ams_id", "string", "ams_id", "int"),
        ("created_by", "string", "created_by", "string"),
        ("created_by_user_id", "int", "created_by_user_id", "int"),
        ("user_id", "int", "user_id", "int"),
        ("broker_type", "string", "broker_type", "string"),
        ("brokerage_id", "int", "brokerage_id", "int"),
        ("updated", "timestamp", "updated", "timestamp"),
    ],
    transformation_ctx="map_brokers_fields_node1649876718627",
)

# Script generated for node Join
clients_client_assignees_node1666988548566DF = (
    clients_client_assignees_node1666988548566.toDF()
)
RenamedkeysforJoin_node1666989032763DF = RenamedkeysforJoin_node1666989032763.toDF()
Join_node1666988685850 = DynamicFrame.fromDF(
    clients_client_assignees_node1666988548566DF.join(
        RenamedkeysforJoin_node1666989032763DF,
        (
            clients_client_assignees_node1666988548566DF["broker_id"]
            == RenamedkeysforJoin_node1666989032763DF["`(right) id`"]
        ),
        "leftsemi",
    ),
    glueContext,
    "Join_node1666988685850",
)

# Script generated for node map_clients_client_fields
map_clients_client_fields_node1649876958795 = ApplyMapping.apply(
    frame=select_clients_client_fields_node1649876905884,
    mappings=[
        ("opportunity_type", "string", "opportunity_type", "string"),
        ("ams_lookup_code", "string", "ams_lookup_code", "string"),
        ("is_active", "boolean", "is_active", "boolean"),
        ("created", "timestamp", "created", "timestamp"),
        ("ams_id", "string", "ams_id", "int"),
        ("type", "string", "type", "string"),
        ("primary_user_id", "int", "primary_user_id", "int"),
        ("can_view_policies", "boolean", "can_view_policies", "boolean"),
        ("last_activity", "timestamp", "last_activity", "timestamp"),
        ("name", "string", "name", "string"),
        ("id", "int", "id", "int"),
        ("created_by_id", "int", "created_by_id", "int"),
        ("updated", "timestamp", "updated", "timestamp"),
        ("brokerage_id", "int", "brokerage_id", "int"),
        ("sic_code_id", "string", "sic_code_id", "string"),
    ],
    transformation_ctx="map_clients_client_fields_node1649876958795",
)

# Script generated for node client_id_only
client_id_only_node1666990931338 = ApplyMapping.apply(
    frame=select_clients_client_fields_node1649876905884,
    mappings=[("id", "int", "id_of_client", "int")],
    transformation_ctx="client_id_only_node1666990931338",
)

# Script generated for node join_brokerage_forms_form_builder_forms
form_builder_form_node1649878282686DF = form_builder_form_node1649878282686.toDF()
select_form_fields_node1649878250458DF = select_form_fields_node1649878250458.toDF()
join_brokerage_forms_form_builder_forms_node1649878304116 = DynamicFrame.fromDF(
    form_builder_form_node1649878282686DF.join(
        select_form_fields_node1649878250458DF,
        (
            form_builder_form_node1649878282686DF["id"]
            == select_form_fields_node1649878250458DF["form_id"]
        ),
        "leftsemi",
    ),
    glueContext,
    "join_brokerage_forms_form_builder_forms_node1649878304116",
)

# Script generated for node write_regions_region
write_regions_region_node1649878739586 = write_regions_region(
    glueContext,
    DynamicFrameCollection(
        {
            "select_region_fields_node1649878704798": select_region_fields_node1649878704798
        },
        glueContext,
    ),
)

# Script generated for node join_regions_client_regions
join_regions_client_regions_node1649878801433 = Join.apply(
    frame1=clients_client_regions_node1649878772765,
    frame2=select_region_fields_node1649878704798,
    keys1=["region_id"],
    keys2=["guid"],
    transformation_ctx="join_regions_client_regions_node1649878801433",
)

# Script generated for node join_regions_broker_regions
join_regions_broker_regions_node1649878932961 = Join.apply(
    frame1=brokers_broker_regions_node1649878913932,
    frame2=select_region_fields_node1649878704798,
    keys1=["region_id"],
    keys2=["guid"],
    transformation_ctx="join_regions_broker_regions_node1649878932961",
)

# Script generated for node write_responses_responsevalue
write_responses_responsevalue_node1649879868716 = write_responses_responsevalue(
    glueContext,
    DynamicFrameCollection(
        {
            "select_response_values_fields_node1649879839634": select_response_values_fields_node1649879839634
        },
        glueContext,
    ),
)


# Script generated for node Join
submissions_formcomment_node1666992103407DF = (
    submissions_formcomment_node1666992103407.toDF()
)
map_auth_fields_node1667513245257DF = map_auth_fields_node1667513245257.toDF()
Join_node1666992242158 = DynamicFrame.fromDF(
    submissions_formcomment_node1666992103407DF.join(
        map_auth_fields_node1667513245257DF,
        (
            submissions_formcomment_node1666992103407DF["user_id"]
            == map_auth_fields_node1667513245257DF["id_of_user"]
        ),
        "leftsemi",
    ),
    glueContext,
    "Join_node1666992242158",
)


# Script generated for node write_brokers_broker
write_brokers_broker_node1649876773099 = write_brokers_broker(
    glueContext,
    DynamicFrameCollection(
        {"map_brokers_fields_node1649876718627": map_brokers_fields_node1649876718627},
        glueContext,
    ),
)

# Script generated for node Select Fields
SelectFields_node1666989170243 = SelectFields.apply(
    frame=Join_node1666988685850,
    paths=["broker_id", "id", "client_id", "servicing_role", "last_viewed_documents"],
    transformation_ctx="SelectFields_node1666989170243",
)


# Script generated for node write_clients_client
write_clients_client_node1649876998550 = write_clients_client(
    glueContext,
    DynamicFrameCollection(
        {
            "map_clients_client_fields_node1649876958795": map_clients_client_fields_node1649876958795
        },
        glueContext,
    ),
)

# Script generated for node rename_clients_client_keys
rename_clients_client_keys_node1649877142147 = ApplyMapping.apply(
    frame=map_clients_client_fields_node1649876958795,
    mappings=[("id", "int", "clients_client_id", "int")],
    transformation_ctx="rename_clients_client_keys_node1649877142147",
)

# Script generated for node Join
clients_clientcontact_node1666990877010DF = (
    clients_clientcontact_node1666990877010.toDF()
)
client_id_only_node1666990931338DF = client_id_only_node1666990931338.toDF()
Join_node1666991042529 = DynamicFrame.fromDF(
    clients_clientcontact_node1666990877010DF.join(
        client_id_only_node1666990931338DF,
        (
            clients_clientcontact_node1666990877010DF["client_id"]
            == client_id_only_node1666990931338DF["id_of_client"]
        ),
        "leftsemi",
    ),
    glueContext,
    "Join_node1666991042529",
)


# Script generated for node select_form_builder_form_fields
select_form_builder_form_fields_node1649878377763 = SelectFields.apply(
    frame=join_brokerage_forms_form_builder_forms_node1649878304116,
    paths=[
        "is_private",
        "country",
        "created",
        "description",
        "label",
        "is_live",
        "guid",
        "alias",
        "id",
        "state",
        "updated",
    ],
    transformation_ctx="select_form_builder_form_fields_node1649878377763",
)

# Script generated for node select_client_regions_fields
select_client_regions_fields_node1649878850612 = SelectFields.apply(
    frame=join_regions_client_regions_node1649878801433,
    paths=["region_id", "id", "client_id"],
    transformation_ctx="select_client_regions_fields_node1649878850612",
)

# Script generated for node select_broker_regions_fields
select_broker_regions_fields_node1649878976133 = SelectFields.apply(
    frame=join_regions_broker_regions_node1649878932961,
    paths=["broker_id", "region_id", "id"],
    transformation_ctx="select_broker_regions_fields_node1649878976133",
)

# Script generated for node Custom Transform
CustomTransform_node1666992789654 = write_submissions_formcomment(
    glueContext,
    DynamicFrameCollection(
        {"Join_node1666992242158": Join_node1666992242158}, glueContext
    ),
)
# Script generated for node Custom Transform
CustomTransform_node1666989250232 = write_clients_client_assignees(
    glueContext,
    DynamicFrameCollection(
        {"SelectFields_node1666989170243": SelectFields_node1666989170243}, glueContext
    ),
)


# Script generated for node join_clients_cliententity
join_clients_cliententity_node1649877223315 = Join.apply(
    frame1=client_entities_cliententity_node1649877100779,
    frame2=rename_clients_client_keys_node1649877142147,
    keys1=["client_id"],
    keys2=["clients_client_id"],
    transformation_ctx="join_clients_cliententity_node1649877223315",
)

# Script generated for node join_clients_response_rows
responses_responserow_node1649879157296DF = (
    responses_responserow_node1649879157296.toDF()
)
rename_clients_client_keys_node1649877142147DF = (
    rename_clients_client_keys_node1649877142147.toDF()
)
join_clients_response_rows_node1649879185182 = DynamicFrame.fromDF(
    responses_responserow_node1649879157296DF.join(
        rename_clients_client_keys_node1649877142147DF,
        (
            responses_responserow_node1649879157296DF["client_id"]
            == rename_clients_client_keys_node1649877142147DF["clients_client_id"]
        ),
        "leftsemi",
    ),
    glueContext,
    "join_clients_response_rows_node1649879185182",
)

# Script generated for node join_clients_responses
responses_response_node1649879323666DF = responses_response_node1649879323666.toDF()
rename_clients_client_keys_node1649877142147DF = (
    rename_clients_client_keys_node1649877142147.toDF()
)
join_clients_responses_node1649879342945 = DynamicFrame.fromDF(
    responses_response_node1649879323666DF.join(
        rename_clients_client_keys_node1649877142147DF,
        (
            responses_response_node1649879323666DF["client_id"]
            == rename_clients_client_keys_node1649877142147DF["clients_client_id"]
        ),
        "leftsemi",
    ),
    glueContext,
    "join_clients_responses_node1649879342945",
)

# Script generated for node join_clients_submissions
submissions_submission_node1649879933147DF = (
    submissions_submission_node1649879933147.toDF()
)
rename_clients_client_keys_node1649877142147DF = (
    rename_clients_client_keys_node1649877142147.toDF()
)
join_clients_submissions_node1649879971445 = DynamicFrame.fromDF(
    submissions_submission_node1649879933147DF.join(
        rename_clients_client_keys_node1649877142147DF,
        (
            submissions_submission_node1649879933147DF["client_id"]
            == rename_clients_client_keys_node1649877142147DF["clients_client_id"]
        ),
        "leftsemi",
    ),
    glueContext,
    "join_clients_submissions_node1649879971445",
)

# Script generated for node Drop Fields
DropFields_node1666991185352 = DropFields.apply(
    frame=Join_node1666991042529,
    paths=["id_of_client"],
    transformation_ctx="DropFields_node1666991185352",
)

# Script generated for node write_form_builder_form
write_form_builder_form_node1649878447766 = write_form_builder_form(
    glueContext,
    DynamicFrameCollection(
        {
            "select_form_builder_form_fields_node1649878377763": select_form_builder_form_fields_node1649878377763
        },
        glueContext,
    ),
)

# Script generated for node write_clients_client_regions
write_clients_client_regions_node1649878884762 = write_clients_client_regions(
    glueContext,
    DynamicFrameCollection(
        {
            "select_client_regions_fields_node1649878850612": select_client_regions_fields_node1649878850612
        },
        glueContext,
    ),
)

# Script generated for node write_brokers_broker_regions
write_brokers_broker_regions_node1649878995096 = write_brokers_broker_regions(
    glueContext,
    DynamicFrameCollection(
        {
            "select_broker_regions_fields_node1649878976133": select_broker_regions_fields_node1649878976133
        },
        glueContext,
    ),
)

# Script generated for node select_cliententity_fields
select_cliententity_fields_node1649877278849 = SelectFields.apply(
    frame=join_clients_cliententity_node1649877223315,
    paths=["created", "name", "guid", "type", "updated", "client_id"],
    transformation_ctx="select_cliententity_fields_node1649877278849",
)

# Script generated for node select_client_fields_from_responserows
select_client_fields_from_responserows_node1649879232312 = SelectFields.apply(
    frame=join_clients_response_rows_node1649879185182,
    paths=[
        "deleted",
        "created",
        "ams_id",
        "guid",
        "id",
        "entity_id",
        "author_id",
        "updated",
        "client_id",
    ],
    transformation_ctx="select_client_fields_from_responserows_node1649879232312",
)

# Script generated for node select_client_fields_from_responses
select_client_fields_from_responses_node1649879392447 = SelectFields.apply(
    frame=join_clients_responses_node1649879342945,
    paths=[
        "deleted",
        "created",
        "form_field_id",
        "form_bundle_id",
        "id",
        "response_row_id",
        "author_id",
        "updated",
        "client_id",
        "_value_id",
    ],
    transformation_ctx="select_client_fields_from_responses_node1649879392447",
)

# Script generated for node select_client_submissions_fields
select_client_submissions_fields_node1649880016246 = SelectFields.apply(
    frame=join_clients_submissions_node1649879971445,
    paths=[
        "created",
        "due_date",
        "client_id",
        "sent_to_client_date",
        "deleted",
        "name",
        "effective_date",
        "smart_reminder_frequency",
        "base_submission_id",
        "id",
        "reminder_enabled",
        "created_by_id",
        "state",
        "updated",
    ],
    transformation_ctx="select_client_submissions_fields_node1649880016246",
)
# Script generated for node Custom Transform clients_clientcontact
CustomTransformclients_clientcontact_node1666991208443 = write_clients_clientcontact(
    glueContext,
    DynamicFrameCollection(
        {"DropFields_node1666991185352": DropFields_node1666991185352}, glueContext
    ),
)

# Script generated for node write_client_entities_cliententity
write_client_entities_cliententity_node1649877316598 = write_client_entities_cliententity(
    glueContext,
    DynamicFrameCollection(
        {
            "select_cliententity_fields_node1649877278849": select_cliententity_fields_node1649877278849
        },
        glueContext,
    ),
)

# Script generated for node write_responses_responserow
write_responses_responserow_node1649879254882 = write_responses_responserow(
    glueContext,
    DynamicFrameCollection(
        {
            "select_client_fields_from_responserows_node1649879232312": select_client_fields_from_responserows_node1649879232312
        },
        glueContext,
    ),
)

# Script generated for node write_responses_response
write_responses_response_node1649879418847 = write_responses_response(
    glueContext,
    DynamicFrameCollection(
        {
            "select_client_fields_from_responses_node1649879392447": select_client_fields_from_responses_node1649879392447
        },
        glueContext,
    ),
)

# Script generated for node write_submissions_submission
write_submissions_submission_node1649880059229 = write_submissions_submission(
    glueContext,
    DynamicFrameCollection(
        {
            "select_client_submissions_fields_node1649880016246": select_client_submissions_fields_node1649880016246
        },
        glueContext,
    ),
)

# Script generated for node rename_submissions_submission_keys
rename_submissions_submission_keys_node1649880130847 = ApplyMapping.apply(
    frame=select_client_submissions_fields_node1649880016246,
    mappings=[("id", "int", "submissions_submission_id", "int")],
    transformation_ctx="rename_submissions_submission_keys_node1649880130847",
)

# Script generated for node join_submissions_formbundles
join_submissions_formbundles_node1649880265065 = Join.apply(
    frame1=submission_formbundle_node1649880202064,
    frame2=rename_submissions_submission_keys_node1649880130847,
    keys1=["submission_id"],
    keys2=["submissions_submission_id"],
    transformation_ctx="join_submissions_formbundles_node1649880265065",
)

# Script generated for node join_submission_response_rows
join_submission_response_rows_node1649880455142 = Join.apply(
    frame1=submissions_submission_response_rows_node1649880414463,
    frame2=rename_submissions_submission_keys_node1649880130847,
    keys1=["submission_id"],
    keys2=["submissions_submission_id"],
    transformation_ctx="join_submission_response_rows_node1649880455142",
)

# Script generated for node join_submissions_schedules
join_submissions_schedules_node1649880660661 = Join.apply(
    frame1=select_schedules_schedule_fields_node1649880622626,
    frame2=rename_submissions_submission_keys_node1649880130847,
    keys1=["submission_id"],
    keys2=["submissions_submission_id"],
    transformation_ctx="join_submissions_schedules_node1649880660661",
)

# Script generated for node select_submissions_fields_from_formbundles
select_submissions_fields_from_formbundles_node1649880309243 = SelectFields.apply(
    frame=join_submissions_formbundles_node1649880265065,
    paths=[
        "last_export_type",
        "last_import_type",
        "is_active",
        "created",
        "last_exported",
        "submission_id",
        "client_entity_id",
        "guid",
        "updated",
        "last_imported",
    ],
    transformation_ctx="select_submissions_fields_from_formbundles_node1649880309243",
)

# Script generated for node select_submissions_submission_fields_from_submission_response_rows
select_submissions_submission_fields_from_submission_response_rows_node1649880537312 = SelectFields.apply(
    frame=join_submission_response_rows_node1649880455142,
    paths=["submission_id", "responserow_id", "id"],
    transformation_ctx="select_submissions_submission_fields_from_submission_response_rows_node1649880537312",
)

# Script generated for node drop_submissions_submission_fields_from_schedules
drop_submissions_submission_fields_from_schedules_node1649880724461 = DropFields.apply(
    frame=join_submissions_schedules_node1649880660661,
    paths=["submissions_submission_id"],
    transformation_ctx="drop_submissions_submission_fields_from_schedules_node1649880724461",
)

# Script generated for node write_submissions_formbundle
write_submissions_formbundle_node1649880363848 = write_submissions_formbundle(
    glueContext,
    DynamicFrameCollection(
        {
            "select_submissions_fields_from_formbundles_node1649880309243": select_submissions_fields_from_formbundles_node1649880309243
        },
        glueContext,
    ),
)

# Script generated for node rename_formbundle_keys
rename_formbundle_keys_node1649881688458 = ApplyMapping.apply(
    frame=select_submissions_fields_from_formbundles_node1649880309243,
    mappings=[("guid", "string", "formbundle_guid", "string")],
    transformation_ctx="rename_formbundle_keys_node1649881688458",
)

# Script generated for node write_submissions_submission_response_rows
write_submissions_submission_response_rows_node1649880574441 = write_submissions_submission_response_rows(
    glueContext,
    DynamicFrameCollection(
        {
            "select_submissions_submission_fields_from_submission_response_rows_node1649880537312": select_submissions_submission_fields_from_submission_response_rows_node1649880537312
        },
        glueContext,
    ),
)

# Script generated for node write_schedules_schedule
write_schedules_schedule_node1649880749524 = write_schedules_schedule(
    glueContext,
    DynamicFrameCollection(
        {
            "drop_submissions_submission_fields_from_schedules_node1649880724461": drop_submissions_submission_fields_from_schedules_node1649880724461
        },
        glueContext,
    ),
)

# Script generated for node rename_schedules_schedule_keys
rename_schedules_schedule_keys_node1649886083070 = ApplyMapping.apply(
    frame=drop_submissions_submission_fields_from_schedules_node1649880724461,
    mappings=[("id", "int", "schedules_schedule_id", "int")],
    transformation_ctx="rename_schedules_schedule_keys_node1649886083070",
)

# Script generated for node join_formbundle_forms
join_formbundle_forms_node1649881731676 = Join.apply(
    frame1=rename_formbundle_keys_node1649881688458,
    frame2=select_submission_form_fields_node1649881533507,
    keys1=["formbundle_guid"],
    keys2=["form_bundle_id"],
    transformation_ctx="join_formbundle_forms_node1649881731676",
)

# Script generated for node join_schedules_schedulesentity
join_schedules_schedulesentity_node1649886126047 = Join.apply(
    frame1=schedules_scheduleentity_node1649886037198,
    frame2=rename_schedules_schedule_keys_node1649886083070,
    keys1=["schedule_id"],
    keys2=["schedules_schedule_id"],
    transformation_ctx="join_schedules_schedulesentity_node1649886126047",
)

# Script generated for node drop_formbundle_fields
drop_formbundle_fields_node1649881781776 = DropFields.apply(
    frame=join_formbundle_forms_node1649881731676,
    paths=["formbundle_guid"],
    transformation_ctx="drop_formbundle_fields_node1649881781776",
)

# Script generated for node select_schedulesentity_fields
select_schedulesentity_fields_node1649886192736 = SelectFields.apply(
    frame=join_schedules_schedulesentity_node1649886126047,
    paths=["reviewed_by_client", "alias", "id", "entity_id", "schedule_id"],
    transformation_ctx="select_schedulesentity_fields_node1649886192736",
)

# Script generated for node write_submissions_submissionform
write_submissions_submissionform_node1649881816659 = write_submissions_submissionform(
    glueContext,
    DynamicFrameCollection(
        {
            "drop_formbundle_fields_node1649881781776": drop_formbundle_fields_node1649881781776
        },
        glueContext,
    ),
)

# Script generated for node rename_submission_form_keys
rename_submission_form_keys_node1649885833354 = ApplyMapping.apply(
    frame=drop_formbundle_fields_node1649881781776,
    mappings=[("id", "int", "submissions_submissionform_id", "int")],
    transformation_ctx="rename_submission_form_keys_node1649885833354",
)

# Script generated for node write_schedules_scheduleentity
write_schedules_scheduleentity_node1649886222713 = write_schedules_scheduleentity(
    glueContext,
    DynamicFrameCollection(
        {
            "select_schedulesentity_fields_node1649886192736": select_schedulesentity_fields_node1649886192736
        },
        glueContext,
    ),
)

# Script generated for node join_submission_forms_response_rows
join_submission_forms_response_rows_node1649885785503 = Join.apply(
    frame1=submissions_submissionformresponserow_node1649885761419,
    frame2=rename_submission_form_keys_node1649885833354,
    keys1=["submission_form_id"],
    keys2=["submissions_submissionform_id"],
    transformation_ctx="join_submission_forms_response_rows_node1649885785503",
)

# Script generated for node select_submission_submissionform_fields
select_submission_submissionform_fields_node1649885921418 = SelectFields.apply(
    frame=join_submission_forms_response_rows_node1649885785503,
    paths=["added_date", "removed_date", "submission_form_id", "id", "response_row_id"],
    transformation_ctx="select_submission_submissionform_fields_node1649885921418",
)

# Script generated for node write_submissions_submissionformresponserow
write_submissions_submissionformresponserow_node1649885945733 = write_submissions_submissionformresponserow(
    glueContext,
    DynamicFrameCollection(
        {
            "select_submission_submissionform_fields_node1649885921418": select_submission_submissionform_fields_node1649885921418
        },
        glueContext,
    ),
)

job.commit()
