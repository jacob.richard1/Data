import sys
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job

## @params: [TempDir, JOB_NAME]
args = getResolvedOptions(sys.argv, ['TempDir','JOB_NAME'])

sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args['JOB_NAME'], args)

datasource0 = glueContext.create_dynamic_frame.from_catalog(
    database = "indio-production-backup",
    table_name = "auditing_transactionauthor",
    transformation_ctx = "datasource0",
    additional_options = {'hashexpression': 'txid','hashpartitions': '13'}
)

# Script generated for node Filter
#filter_node = Filter.apply(
#    frame=datasource0,
#    f=lambda row: (
#        bool(re.match("brokerage_gallagher", row["db_name"]))
#        and bool(re.match("REQUEST", row["transaction_type"]))
#    ),
#    transformation_ctx="filter_node",
#)

#applymapping1 = ApplyMapping.apply(
#    frame = filter_node, mappings = [
#        ("staff_author_id", "int", "staff_author_id", "int"),
#        ("txid", "int", "txid", "int"),
#        ("author_id", "int", "author_id", "int"),
#        ("db_name", "string", "db_name", "string"),
#    ],
#    transformation_ctx = "applymapping1")

datasink2 = glueContext.write_dynamic_frame.from_jdbc_conf(
    #frame = applymapping1, 
    frame=datasource0,
    catalog_connection = "analytics", 
    connection_options = {"dbtable": "auditing_transactionauthor", "preactions": "TRUNCATE TABLE auditing_transactionauthor", "database": "indio", "bulkSize": "13"},
    redshift_tmp_dir = args["TempDir"],
    transformation_ctx = "datasink2")

job.commit()