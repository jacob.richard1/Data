
AWS Glue configuration

AWS Glue <-> GitHub <-> GitLab

This GitLab Repo pushes to Github through ssh:
    project settings > repository > mirror > ssh://git@github.com/jacobRichardIndio/Data.git
This GitLab Repo pulls from Github through GitHub actions
    see the .github folder.

Password, username, token to the GitHub is in 1password.
    Contact Jacob Richard, Marius, Rob, or Luis. It is saved under a vault called "AWS Glue GitHub"

Folders should have the same name as the job name

Due to limitations in AWS, you can push as many jobs as you want,
but you can only pull 1 job at a time.

Push and pull (only works on main branch):
    To neither push nor pull from Glue set the name equal to nothing. For example
    brokers_broker=
    To pull a job from Glue, add it to the config file equal to 0. This can take up to 30 minutes. For example
    brokers_broker=0
    To push a job to Glue, add it to the config file equal to 1. This can take up to 30 minutes. For example
    brokers_broker=1
