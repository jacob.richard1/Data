import sys
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job

## @params: [TempDir, JOB_NAME]
args = getResolvedOptions(sys.argv, ['TempDir','JOB_NAME'])

sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args['JOB_NAME'], args)

datasource0 = glueContext.create_dynamic_frame.from_catalog(
    database = "indio-production-backup",
    table_name = "brokers_broker__audit",
    transformation_ctx = "datasource0",
    additional_options = {"jobBookmarkKeys":["tx_issued_at", "event_id"],"jobBookmarkKeysSortOrder":"asc", 'hashexpression': 'id','hashpartitions': '4'}
)

filter_brokers = Filter.apply(
    frame=datasource0,
    f=lambda row: (row["brokerage_id"] == 540),
    transformation_ctx="filter_brokers",
)

applymapping1 = ApplyMapping.apply(
    frame = filter_brokers, mappings = [
        ("event_id", "int", "event_id", "int"),
        ("event_kind", "string", "event_kind", "string"),
        ("tx_issued_at", "timestamp", "tx_issued_at", "timestamp"),
        ("id", "int", "id", "int"),
        ("created", "timestamp", "created", "timestamp"),
        ("updated", "timestamp", "updated", "timestamp"),
        ("created_by", "string", "created_by", "string"),
        ("title", "string", "title", "string"),
        ("phone", "string", "phone", "string"),
        ("brokerage_address", "string", "brokerage_address", "string"),
        ("city_state_zip", "string", "city_state_zip", "string"),
        ("photo", "string", "photo", "string"),
        ("photo_cropped", "string", "photo_cropped", "string"),
        ("photo_metadata", "string", "photo_metadata", "string"),
        ("show_my_profile", "boolean", "show_my_profile", "boolean"),
        ("email_notifications", "string", "email_notifications", "string"),
        ("email_signature_settings", "string", "email_signature_settings", "string"),
        ("broker_type", "string", "broker_type", "string"),
        ("access_type", "string", "access_type", "string"),
        ("access_level", "string", "access_level", "string"),
        ("is_active", "boolean", "is_active", "boolean"),
        ("is_pending", "boolean", "is_pending", "boolean"),
        ("date_deactivated", "timestamp", "date_deactivated", "timestamp"),
        ("date_invited", "timestamp", "date_invited", "timestamp"),
        ("nylas_email_settings", "string", "nylas_email_settings", "string"),
        (
            "encrypted_nylas_access_code",
            "string",
            "encrypted_nylas_access_code",
            "string",
        ),
        (
            "client_activity_emails_enabled",
            "boolean",
            "client_activity_emails_enabled",
            "boolean",
        ),
        ("daily_email_enabled", "boolean", "daily_email_enabled", "boolean"),
        (
            "indio_access_requests_enabled",
            "boolean",
            "indio_access_requests_enabled",
            "boolean",
        ),
        ("ams_id", "string", "ams_id", "string"),
        ("salesforce_contact_id", "string", "salesforce_contact_id", "string"),
        ("default_language", "string", "default_language", "string"),
        ("brokerage_id", "int", "brokerage_id", "int"),
        ("created_by_user_id", "int", "created_by_user_id", "int"),
        ("tx_id", "int", "tx_id", "int"),
        ("user_id", "int", "user_id", "int"),
    ],
    transformation_ctx = "applymapping1")

datasink2 = glueContext.write_dynamic_frame.from_jdbc_conf(
    frame = applymapping1, catalog_connection = "analytics",
    connection_options = {"dbtable": "brokers_broker__audit", "database": "indio", "bulkSize": "2"},
    redshift_tmp_dir = args["TempDir"],
    transformation_ctx = "datasink2")

job.commit()
