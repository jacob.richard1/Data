import sys
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job

## @params: [TempDir, JOB_NAME]
args = getResolvedOptions(sys.argv, ['TempDir','JOB_NAME'])

sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args['JOB_NAME'], args)

datasource0 = glueContext.create_dynamic_frame.from_catalog(
    database = "indio-production-backup",
    table_name = "clients_clientcontact__audit",
    transformation_ctx = "datasource0",
    additional_options = {"jobBookmarkKeys":["tx_issued_at", "event_id"],"jobBookmarkKeysSortOrder":"asc", 'hashexpression': 'tx_id','hashpartitions': '10'}
)

applymapping1 = ApplyMapping.apply(
    frame = datasource0, mappings = [
        ("event_id", "int", "event_id", "int"),
        ("event_kind", "string", "event_kind", "string"),
        ("tx_issued_at", "timestamp", "tx_issued_at", "timestamp"),
        ("created", "timestamp", "created", "timestamp"),
        ("updated", "timestamp", "updated", "timestamp"),
        ("guid", "string", "guid", "string"),
        ("permission", "string", "permission", "string"),
        ("can_view_documents", "boolean", "can_view_documents", "boolean"),
        ("salesforce_id", "string", "salesforce_id", "string"),
        ("ams_id", "string", "ams_id", "string"),
        ("default_language", "string", "default_language", "string"),
        ("client_id", "int", "client_id", "int"),
        ("tx_id", "int", "tx_id", "int"),
        ("user_id", "int", "user_id", "int"),
    ],
    transformation_ctx = "applymapping1")

datasink2 = glueContext.write_dynamic_frame.from_jdbc_conf(
    frame = applymapping1, catalog_connection = "analytics",
    connection_options = {"dbtable": "clients_clientcontact__audit", "database": "indio", "bulkSize": "10"},
    redshift_tmp_dir = args["TempDir"],
    transformation_ctx = "datasink2")

job.commit()